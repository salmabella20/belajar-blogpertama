-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2020 at 07:36 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajarblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok DiSajikan saat HUT RI ke-75', 'blablabla', NULL, NULL, NULL, 'HUT RI Ke-75'),
(2, 'Prof.', 'Nobis autem mollitia quam. Ipsa et accusamus natus et maiores. Veritatis veniam ea labore est est possimus rerum.', '2020-09-17 20:00:08', '2020-09-17 20:00:08', NULL, 'Meat Packer'),
(3, 'Mrs.', 'In nobis et numquam consequatur enim. Officiis quam blanditiis voluptatum iusto nobis porro ad culpa. Laborum accusamus similique nesciunt quisquam eius error et.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Electrical Engineering Technician'),
(4, 'Prof.', 'Numquam molestiae aut dicta hic eos incidunt. Quia sunt ut debitis omnis. Tempora adipisci nulla laborum corrupti sed neque aliquid. Enim vitae animi quaerat distinctio est animi praesentium.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Tax Examiner'),
(5, 'Dr.', 'Sed consequuntur qui iste eaque iure. Temporibus illo velit quas neque corrupti voluptatum. Debitis libero minus velit dolor minus.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Insurance Investigator'),
(6, 'Dr.', 'Distinctio laborum exercitationem est modi est. Occaecati architecto consequatur ipsum. Esse a omnis itaque provident eveniet. Rerum dolorum numquam aspernatur.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Word Processors and Typist'),
(7, 'Prof.', 'Quis provident laborum dolore nam libero. Doloribus iste vel dolore non saepe alias consectetur. Possimus numquam quisquam vero et sed.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Emergency Medical Technician and Paramedic'),
(8, 'Dr.', 'Provident rem soluta ea voluptatem est placeat. Dolor consequatur enim maiores minima natus. Sit eaque tenetur inventore impedit numquam sed. Voluptate corrupti nesciunt sed recusandae ullam atque.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Meter Mechanic'),
(9, 'Prof.', 'Molestiae molestiae voluptatibus expedita aspernatur. Possimus voluptas voluptates et magni minima culpa accusamus. A architecto minus dolore impedit alias voluptatem quod.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Receptionist and Information Clerk'),
(10, 'Dr.', 'Corrupti et in quis est provident itaque. Ducimus asperiores nisi laborum laudantium quam. Perspiciatis et sunt corrupti adipisci ducimus.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Cafeteria Cook'),
(11, 'Dr.', 'Non qui doloremque assumenda debitis ut consequatur. Cupiditate fugiat voluptas eum distinctio.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Food Preparation'),
(12, 'Miss', 'Ut labore ab a. Enim voluptate et dignissimos voluptatem provident reiciendis.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Washing Equipment Operator'),
(13, 'Prof.', 'Earum quia asperiores voluptatem iusto. Reiciendis illum occaecati sed qui. Molestiae occaecati molestias non deleniti optio itaque recusandae. Nisi velit corporis vel consequatur id sequi.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Gas Pumping Station Operator'),
(14, 'Miss', 'Suscipit nihil voluptas fugiat repudiandae consequatur doloribus vel. Enim est veritatis consectetur mollitia harum libero minus et.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Occupational Therapist Aide'),
(15, 'Ms.', 'Qui ex illo expedita sit ut. Est reiciendis et ullam tempora temporibus. Molestias impedit tempora et suscipit nobis. Dolorum dolorem dolorum modi odio.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Food Preparation'),
(16, 'Prof.', 'Mollitia minus eius veniam harum. Consequatur qui ducimus iure molestias mollitia quos recusandae harum. Veritatis hic aut dolorum perspiciatis illo consequatur.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Movers'),
(17, 'Dr.', 'Assumenda iure earum eligendi veniam ipsum. Sit recusandae fuga eius. Quibusdam accusantium rem illum sit minus. Voluptatem sit asperiores et animi similique cum eum. Et voluptas consectetur a qui nesciunt voluptatem suscipit amet.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Chemical Plant Operator'),
(18, 'Dr.', 'Nemo iste facilis tempora vitae ut. Tempora dolorem asperiores dolores molestiae. Autem voluptatibus reprehenderit veniam quia incidunt accusamus dolores.', '2020-09-17 20:00:09', '2020-09-17 20:00:09', NULL, 'Bus Driver'),
(19, 'Mrs.', 'Et et delectus laboriosam et voluptatem incidunt neque. Nulla tenetur perferendis incidunt non quam. Ut perspiciatis est inventore quis inventore omnis vitae.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Librarian'),
(20, 'Dr.', 'Nisi possimus sint sunt hic nesciunt facilis. Distinctio recusandae non rem dolores repudiandae. Provident dolorem voluptas enim provident et. Error et quaerat commodi omnis in. Maiores est illo aliquam voluptate aut officiis.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Directory Assistance Operator'),
(21, 'Dr.', 'Eligendi natus enim ex dolores facilis. Hic natus est qui dolorum repellat voluptatum. Autem ab dolor est eius hic nemo dicta cum.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Pastry Chef'),
(22, 'Miss', 'Molestiae quo hic temporibus vel consequatur. Et mollitia libero soluta ipsa. Dolor tempora nesciunt ut perspiciatis eligendi. Veniam recusandae et dolorum natus perspiciatis.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Model Maker'),
(23, 'Dr.', 'Aut inventore id in aut. Nobis quia a aut in qui similique. Unde aspernatur rerum repellat perspiciatis ut. Vel saepe exercitationem dolorem a et eos sit.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Graphic Designer'),
(24, 'Dr.', 'Ut quaerat nostrum aliquam dolorum molestias. Eos ea dolores vero. Quo id et qui ut harum dicta necessitatibus. Asperiores illum consequatur quia modi.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Power Plant Operator'),
(25, 'Prof.', 'Quae sapiente eveniet sapiente rerum voluptate nemo unde. Adipisci consequatur dolorem aliquid aliquam quisquam ut dignissimos. Aut qui eum libero architecto ipsam itaque. Ratione et architecto magni natus ea nam neque.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Building Inspector'),
(26, 'Ms.', 'Numquam molestiae id a a ipsa dolor. Accusamus voluptatem voluptates ea pariatur.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Public Health Social Worker'),
(27, 'Miss', 'Modi aut unde ut est esse nesciunt aut. Fugiat ea neque ut atque non ipsum modi illo. Voluptatem temporibus voluptatibus nostrum quis enim quia. Ut facilis consequatur magni enim.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Telephone Station Installer and Repairer'),
(28, 'Dr.', 'Molestiae laudantium nihil ut. Voluptatem qui eveniet beatae doloribus ab ab corrupti. Possimus sed delectus sit assumenda. Voluptatum omnis iure nulla voluptatibus.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Farmer'),
(29, 'Prof.', 'Ea quos sapiente repellendus voluptatem molestiae ipsa consequatur. Incidunt explicabo dolorem illum ipsum eligendi quasi sequi. Consectetur et autem quod reprehenderit cumque. Dignissimos vel iste ab et consequatur molestias dolor. Maxime enim aliquid harum non odio.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Counselor'),
(30, 'Prof.', 'Architecto odit aut nisi quis. Aut quidem fugiat impedit quas est voluptas placeat laudantium. Sit itaque velit numquam sed enim.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Medical Scientists'),
(31, 'Dr.', 'Quia et deserunt autem quos. Nam quasi itaque sed. Ad hic in non consequatur. Voluptatem enim qui ut et ut.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Mail Machine Operator'),
(32, 'Miss', 'Placeat sunt fugiat et dolores. Et consequatur ipsum aspernatur dolore voluptatem laborum fuga. Voluptatum quis tempore quo consequatur at doloremque dolores.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Painter'),
(33, 'Mr.', 'Tenetur aut delectus non ullam. Iusto mollitia ut in voluptas iste facilis ut veritatis. Unde suscipit aut dolorum quod. Quae quidem consequatur quis qui quo.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Licensed Practical Nurse'),
(34, 'Mr.', 'Blanditiis sint similique veritatis aperiam illum. Pariatur commodi libero accusantium expedita a. Quia est sunt accusamus cupiditate quae consequatur. Quod perspiciatis ut aut.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Central Office and PBX Installers'),
(35, 'Prof.', 'Expedita et veritatis et perferendis omnis. Voluptate fugiat voluptatem voluptates natus odio non. Debitis omnis quo labore quam.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Transportation Attendant'),
(36, 'Prof.', 'Iusto modi earum vitae et quisquam. Mollitia aut officia eos. Et vel corrupti atque doloribus. Ut porro omnis animi ut. Necessitatibus omnis deleniti dolores molestiae adipisci voluptatum ad.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Engraver'),
(37, 'Prof.', 'Ut natus amet deleniti ipsum modi. Nam debitis nostrum qui molestiae provident velit hic sed. Sit et aut error aut et quia consequatur.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Agricultural Crop Farm Manager'),
(38, 'Dr.', 'Nam est ut dicta fuga accusantium optio. Voluptatum nihil nesciunt laborum et expedita id quos. Similique et quia debitis cumque.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Hand Presser'),
(39, 'Mrs.', 'Assumenda officia quaerat ut accusamus architecto ut voluptas magni. Ratione sed sunt omnis nesciunt voluptas est. A aspernatur perspiciatis repellat fugiat qui atque placeat.', '2020-09-17 20:00:10', '2020-09-17 20:00:10', NULL, 'Archivist'),
(40, 'Mr.', 'Corrupti doloremque assumenda rem quas natus nobis. Eum exercitationem placeat ea provident ratione et. Et quam mollitia placeat in dolor aut.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Chemical Equipment Controller'),
(41, 'Dr.', 'Magnam ratione dicta sit voluptate aliquid. Eum provident reprehenderit quidem tenetur aspernatur. Excepturi repellat blanditiis ut perferendis. Sit quidem animi praesentium dolorum deleniti labore.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Physical Scientist'),
(42, 'Mr.', 'Laudantium sed rerum perferendis inventore. Neque et consectetur ut aut suscipit porro aut. Facilis ipsum possimus in repellendus itaque.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Organizational Development Manager'),
(43, 'Dr.', 'Sit consequatur quibusdam inventore unde est autem molestias. Quas illum amet est voluptas sunt aliquam. Officiis nostrum laudantium sit quas dolorem quibusdam odit.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Precision Pattern and Die Caster'),
(44, 'Dr.', 'Quia delectus ea et velit facere ab. Esse adipisci quam corporis quas perferendis. Molestias eos repudiandae odio reiciendis sed consequatur quis. Aut ea dolor iste doloremque.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Health Educator'),
(45, 'Dr.', 'Vel sapiente eveniet distinctio autem et rerum deleniti. Cum cupiditate qui aliquid voluptas optio consequuntur omnis. Molestias reiciendis quae atque neque in. Nisi et sint amet cumque est.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Molding Machine Operator'),
(46, 'Prof.', 'Laboriosam vel rerum reiciendis vero culpa omnis. Et totam reprehenderit quod nihil exercitationem. Possimus dicta unde consequatur aut accusamus facere. Numquam in est sed cupiditate nemo.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Business Operations Specialist'),
(47, 'Prof.', 'Sed est illo minus. Illum qui laboriosam inventore quo vitae molestiae eligendi. Voluptatem perspiciatis et sunt unde voluptatem voluptatem. Sed sed numquam officiis quibusdam cupiditate. Labore expedita possimus pariatur ut.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Pharmacy Technician'),
(48, 'Prof.', 'Vero a beatae fuga et. Velit cupiditate non labore quaerat illum veritatis. Provident numquam alias vero.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Public Relations Specialist'),
(49, 'Mrs.', 'Nihil autem et cumque atque hic. Itaque nisi quo quo voluptatibus. Eius ut voluptates illum quos. Exercitationem reiciendis laboriosam exercitationem eius qui voluptates recusandae.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Forming Machine Operator'),
(50, 'Miss', 'Vel laudantium voluptatem aut impedit doloremque quia qui. Earum et dolor sunt quam ut maxime eos. Et consectetur sit non consequatur voluptas voluptatem dolorem veniam. Commodi magnam laborum atque dolorum.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Etcher'),
(51, 'Dr.', 'Iste doloremque earum dolor deleniti. Dolores mollitia repellat qui fuga hic error. Laborum magni qui pariatur laboriosam. Magni ab tempore omnis voluptatum.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Technical Writer'),
(52, 'Mr.', 'Debitis ea quasi est. Aut corrupti velit possimus sit nihil nam. Porro laboriosam assumenda beatae suscipit provident saepe quia.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Engineering Teacher'),
(53, 'Mrs.', 'Molestias laboriosam nisi nemo consequatur et. Et vel commodi itaque maxime error. Quia voluptatem cupiditate dolore ea occaecati quos. Sed laudantium dolore itaque eos.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Political Scientist'),
(54, 'Prof.', 'Nihil deleniti doloribus deserunt laboriosam praesentium voluptas ipsa. Quasi esse illum accusamus distinctio. Voluptatem quae harum quia. Perferendis praesentium molestiae ducimus praesentium.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Internist'),
(55, 'Mr.', 'Maiores voluptatum maiores et eveniet provident. Voluptatem est incidunt quod. Voluptas omnis ut enim autem numquam atque iusto. Illum molestiae magni tempora autem expedita.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Gluing Machine Operator'),
(56, 'Prof.', 'Consectetur dolorem nam aut illum itaque omnis doloremque eum. Qui necessitatibus voluptates occaecati ut et placeat. Voluptatibus in quis magni ea eum officia dolorum illo. Dolor qui maiores mollitia atque hic qui qui.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Electrical and Electronic Inspector and Tester'),
(57, 'Mr.', 'Et quam enim numquam hic. Eum est beatae rerum velit accusantium et aperiam quia. Corrupti eos quae odit modi expedita impedit quas animi. Sunt odio non et.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Aircraft Mechanics OR Aircraft Service Technician'),
(58, 'Ms.', 'Doloribus sit praesentium perferendis quas laboriosam. Et nemo quod labore doloremque libero. Ipsam eaque veniam voluptatem. Porro sed quia placeat qui. Ut eligendi delectus quae tenetur necessitatibus quo.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Mathematician'),
(59, 'Dr.', 'Est et qui commodi maiores. Vel repudiandae placeat maxime. Labore aut reprehenderit placeat reprehenderit.', '2020-09-17 20:00:11', '2020-09-17 20:00:11', NULL, 'Insurance Policy Processing Clerk'),
(60, 'Prof.', 'Nam ipsa sint beatae quas minus delectus fuga. Beatae molestias quia qui non fugiat ex ea minima. Aut laudantium et assumenda quasi incidunt fugit sequi.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Nursing Instructor'),
(61, 'Miss', 'Ex enim perferendis magnam adipisci sed corporis odio. Necessitatibus dolores voluptas natus culpa id soluta quo. Nemo sed non ut ea ut qui.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Command Control Center Officer'),
(62, 'Mr.', 'Voluptates sit culpa culpa corrupti voluptatum mollitia dolor. Dolorem dolorem impedit quasi. Adipisci totam odit consequatur nihil voluptas. Corporis eos nihil illum provident.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Vice President Of Marketing'),
(63, 'Dr.', 'Sunt doloribus est quia quia ab. Sed velit ullam laudantium tenetur fugit quas sequi. Voluptatum eum magni sapiente molestiae natus ut sit. Numquam sed commodi beatae quia quia vero quia. Recusandae qui ea aspernatur animi nemo quas necessitatibus.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Terrazzo Workes and Finisher'),
(64, 'Miss', 'Est deserunt iste est dicta maiores tenetur cupiditate. Sit saepe alias natus. Repudiandae sit rerum sit ab.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'CFO'),
(65, 'Dr.', 'Est corporis doloribus nobis minima aut similique. Et voluptas autem earum unde. Et perspiciatis eius labore consequatur autem tenetur. Et voluptate aut molestiae enim.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Aircraft Mechanics OR Aircraft Service Technician'),
(66, 'Ms.', 'Ea quam in aut voluptatem voluptatem voluptatem. Odio sed voluptas expedita enim ducimus. Adipisci vel voluptatem maxime voluptates aut minima sequi. Eaque id sunt magni eaque.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Machinery Maintenance'),
(67, 'Dr.', 'Ut at assumenda nostrum ad delectus possimus dolorum facilis. Non magnam et earum ea hic. Molestias eaque velit nihil eveniet laborum rerum.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Court Reporter'),
(68, 'Mr.', 'Placeat quo necessitatibus aut dolorem voluptatibus aut provident deleniti. Odio nobis molestias totam nemo cupiditate. Itaque assumenda aut et necessitatibus. Aut temporibus corporis nobis quia laboriosam.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Potter'),
(69, 'Mr.', 'Et et et culpa expedita animi sit et. Ullam saepe saepe qui et modi ut. Repellendus quia velit sit provident. Ipsum exercitationem praesentium officiis esse ut. Odio commodi eaque suscipit voluptatibus rerum perspiciatis ut.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Biologist'),
(70, 'Prof.', 'Amet ut sit illum qui sint aut. Eius esse aut dolore earum expedita. Id iure rerum autem eaque qui.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'New Accounts Clerk'),
(71, 'Prof.', 'Molestiae quod dolorum et quam et. Omnis a error ad reprehenderit itaque rerum. Quibusdam et ut cum.', '2020-09-17 20:00:12', '2020-09-17 20:00:12', NULL, 'Waitress'),
(72, 'Prof.', 'Dolor similique facilis asperiores consectetur praesentium exercitationem. Neque exercitationem culpa maxime ut est laborum. Doloribus qui perspiciatis perferendis consequatur autem dicta nulla.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Organizational Development Manager'),
(73, 'Miss', 'Error optio et aut illo. Et nam similique magni soluta sed. Amet culpa expedita provident.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Civil Engineering Technician'),
(74, 'Prof.', 'Blanditiis eligendi odit corporis et. Sit fuga enim natus et laborum doloremque. Doloribus sed aut occaecati rerum ducimus aut. Rerum reiciendis voluptatibus non odit tempore dicta.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Food Service Manager'),
(75, 'Dr.', 'Ut autem tempora ea qui corporis officia quae doloribus. Sequi iusto ut rerum aut enim. Occaecati ipsa id illum commodi. Est illo et aut rerum sint quibusdam dolorem. Esse molestias quasi accusantium suscipit.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Automotive Body Repairer'),
(76, 'Dr.', 'Eveniet maiores sit est rerum eum quos tempora. Et corporis ullam hic saepe explicabo. Alias nisi ut nesciunt voluptatem voluptate. Corporis consectetur incidunt quod. Ex nesciunt consectetur tempore aut laborum commodi quia.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Etcher and Engraver'),
(77, 'Miss', 'Corrupti eligendi doloremque officia aut laboriosam. Facere in quae velit deleniti sit provident voluptatibus. Soluta dolorum ut inventore.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Sales Person'),
(78, 'Dr.', 'Optio in ut enim vel dolorum eius. Consequuntur hic ex tempora harum ut totam. Sit modi qui rerum repellat.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Human Resource Director'),
(79, 'Miss', 'Eos accusantium consequatur accusamus soluta. Debitis quis et accusantium consequatur itaque. Velit illum blanditiis facilis velit.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Precision Pattern and Die Caster'),
(80, 'Prof.', 'Quasi nemo non laudantium et magni ad. Libero nostrum adipisci quia sunt. Est ea ratione doloribus culpa animi qui et.', '2020-09-17 20:00:13', '2020-09-17 20:00:13', NULL, 'Mechanical Inspector'),
(81, 'Miss', 'Tempore et provident vero fugiat. Dolorem autem aut recusandae corporis molestiae. Quidem eaque porro vero veniam voluptatem aut. Beatae vitae sed incidunt culpa.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Surveying Technician'),
(82, 'Miss', 'Impedit quo corporis consequatur aut temporibus. Incidunt ab necessitatibus quam sint. Iure voluptatibus est aut praesentium in. Voluptatem nisi consequatur consectetur quia dolor ut ratione.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Mine Cutting Machine Operator'),
(83, 'Dr.', 'Sed corporis nihil minima earum et architecto optio. Quos itaque aut odit doloribus expedita magni dolorem. Minima iusto beatae sequi qui.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Recreational Therapist'),
(84, 'Mrs.', 'In non qui debitis repudiandae sed assumenda asperiores. Ea fugit repellat dolorem beatae aperiam dolorem. Non voluptas eveniet numquam est itaque quod animi. Ad doloribus quibusdam voluptatem magnam omnis ea quo. Totam soluta totam sunt perferendis suscipit ex.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Shuttle Car Operator'),
(85, 'Mr.', 'Dolores ea quidem provident optio. Officiis aspernatur facilis voluptates optio.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Self-Enrichment Education Teacher'),
(86, 'Mr.', 'Numquam enim temporibus quidem dolores. Deleniti tempora voluptas temporibus enim est autem fuga. Quia ad vero ut eum sit qui. Quo commodi magnam beatae minus.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Sales Engineer'),
(87, 'Mr.', 'Aut et occaecati ut voluptatem autem. Assumenda recusandae repellendus accusantium. Numquam consequuntur facilis sit culpa est facere.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Webmaster'),
(88, 'Prof.', 'Soluta minima occaecati blanditiis et enim ipsum. Aperiam aliquid odit asperiores eos sed qui. Et est voluptas voluptatum dolorum provident sunt. Delectus quaerat nisi voluptas ab deleniti.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Gaming Surveillance Officer'),
(89, 'Prof.', 'Ipsam fuga optio tempora et ullam veniam. Cum voluptate saepe autem consequatur dicta repudiandae. Modi nam consequatur inventore ut repudiandae dicta ad quisquam. A culpa expedita dignissimos.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Mining Machine Operator'),
(90, 'Prof.', 'Ut deleniti qui explicabo molestiae hic. Et occaecati alias nemo voluptas. Iste expedita iure saepe tempora nulla odit. Sed et numquam sed quis omnis quasi aut facere.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Paperhanger'),
(91, 'Dr.', 'Ea unde cupiditate qui voluptatem ut sint. Dolores ducimus omnis maxime corporis sunt dignissimos in sunt. Deleniti vero assumenda aut. Dolorem quam iusto aliquam quod.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Embalmer'),
(92, 'Dr.', 'Labore aut et tenetur nisi omnis quibusdam et. Aliquid repellendus laudantium ducimus omnis. Sint delectus enim ab mollitia cum quaerat quis. Rem totam harum quod aut ipsam dolorem asperiores.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Landscaping'),
(93, 'Dr.', 'Commodi eius voluptas iure ipsum eum quaerat. Qui exercitationem autem nemo dolorum non.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Barber'),
(94, 'Ms.', 'Architecto illum blanditiis consequatur sunt et. Adipisci maxime vel ut ducimus voluptatum sed accusamus. Dolores et consequatur odit cupiditate sequi optio voluptas sed.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Concierge'),
(95, 'Mrs.', 'Accusantium odit dolorem ea velit amet amet. Aut et dignissimos ex consequatur. Quia deserunt eos quia qui architecto iusto vitae. Exercitationem quia ratione debitis quia et.', '2020-09-17 20:00:14', '2020-09-17 20:00:14', NULL, 'Industrial Safety Engineer'),
(96, 'Dr.', 'Non consequatur molestiae sed nulla exercitationem sapiente. Nihil aut sequi est. Quo aut est voluptate quasi alias. Quas doloremque voluptatem et rerum dolorem sunt velit.', '2020-09-17 20:00:15', '2020-09-17 20:00:15', NULL, 'Operations Research Analyst'),
(97, 'Dr.', 'Cupiditate et vel sunt sequi quidem facere. Exercitationem non earum numquam autem dolorem est in iusto. Dignissimos vero non atque quis dolores qui dolorum ullam.', '2020-09-17 20:00:15', '2020-09-17 20:00:15', NULL, 'Carver'),
(98, 'Mrs.', 'Eum est distinctio omnis fugiat vel eos. Eos eos aut sed molestiae. Ut eos minus doloremque quo qui hic. Atque provident et sit.', '2020-09-17 20:00:15', '2020-09-17 20:00:15', NULL, 'Police and Sheriffs Patrol Officer'),
(99, 'Prof.', 'Voluptatem consectetur eum deserunt exercitationem. Laudantium reprehenderit eum sunt. Aut aspernatur aut dolore error sed exercitationem error.', '2020-09-17 20:00:15', '2020-09-17 20:00:15', NULL, 'Machinist'),
(100, 'Ms.', 'Suscipit reprehenderit quis sapiente sed quod ut. Dolores ut consequuntur qui fugiat ipsum eligendi. Error dolorum quas doloribus nemo eligendi exercitationem et. Dicta aut et vel necessitatibus enim ipsa assumenda.', '2020-09-17 20:00:15', '2020-09-17 20:00:15', NULL, 'Pump Operators'),
(101, 'Prof.', 'Et hic dolorem et rerum dolore culpa non. Enim cum vitae exercitationem sit sit ea autem dolor. Cumque non vel fugiat odit. Quasi vel earum fugit. Quasi aperiam odio quo tempore officia recusandae odio.', '2020-09-17 20:00:15', '2020-09-17 20:00:15', NULL, 'Petroleum Technician');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_08_15_055458_create_blog_table', 1),
(10, '2020_08_15_062312_add_category_in_blog_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

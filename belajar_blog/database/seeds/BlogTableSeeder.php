<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
        	'title'=> '7 Makanan Tradisional Khas Jawa yang Cocok DiSajikan saat HUT RI ke-75',
        	'content'=> 'blablabla',
        	'category'=> 'HUT RI Ke-75' 
        ]);
    }
}
